# -*- coding: utf-8 -*-

import sys
import re
import mechanize
import urllib2
import cookielib
from bs4 import BeautifulSoup
import telepot

# telepot 로 텔레그램 메시지 준비
YOUR_ACCESS_TOKEN = ""  # 여기에 자신의 토큰을 넣으시면 됩니다
mytelegramid=0
bot = telepot.Bot(YOUR_ACCESS_TOKEN)

# 파일에서 서점 책 데이터 읽기

AladinBook = []
AladinShop = []

f1 = open("OffAladin.ini")

idx = 0
tmp = ""

for s in f1:
    if idx == 0:
        idx += 1
        AladinShop = s.split(",")
    elif idx % 2 == 1:
        tmp = s.replace("\n", "").decode('utf-8')
        idx += 1
    elif idx % 2 == 0:
        AladinBook.append((tmp, s.replace("\n", "")))
        idx += 1

# 디버그를 위해 데이터를 한번 뿌려주지만 삭제해도 됩니다
print "Shop is "

for i in range(len(AladinShop)):
    AladinShop[i] = AladinShop[i].replace(" ", "").replace("\n", "")
    print AladinShop[i].decode('utf-8')
print "Book is "
for i in range(len(AladinBook)):
    print AladinBook[i][0]
    print "Browsing Starting "+AladinBook[i][1]
# mechanize 로 브라우저에 접속

cj = cookielib.CookieJar()
br = mechanize.Browser()
br.set_cookiejar(cj)
br.set_handle_robots(False)

for i in range(len(AladinBook)):
    weburl = ("http://www.aladin.co.kr/search/wsearchresult.aspx?SearchTarget=UsedStore&SearchWord=" + AladinBook[i][1] + "&x=0&y=0")
    br.open(weburl)
    mobileurl = "http://www.aladin.co.kr/m/msearch.aspx?SearchTarget=UsedStore&SearchWord=" + AladinBook[i][0].replace(" ","+")
    print "web url : %d "%i +weburl

    soup = BeautifulSoup(br.response().read(),"html5lib")
    #soup = BeautifulSoup(br.response().read())

    #print soup

    # <b class="bo3"> 책 제목
    # <div class="ss_book_list"><ul>
    # <div class="usedshop_off_text2_box">
    # bcols = soup.findAll( 'b', attrs={ 'class' : "bo3" } )

    cols = soup.findAll('a', attrs={'class': "usedshop_off_text3"})

    #print AladinBook[i][0]

    for col in cols:
        #print col.text
        for shop in AladinShop:
            if col.text.find(shop.decode("utf-8")) >= 0:
                print "telegram send message : " + col.text + u"에 " + AladinBook[i][0] + u" 입고 되었습니다 " + weburl
                bot.sendMessage( mytelegramid, col.text + u"에 " + AladinBook[i][0] + u" 입고 되었습니다 "+ mobileurl)

#send message

#Bot
#bot.sendMessage(2360, " 입고 되었습니다")
#http://www.aladin.co.kr/shop/wproduct.aspx?ItemId=2227082